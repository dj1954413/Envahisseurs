from PIL import ImageTk
from tkinter import *
from random import *
import pyglet, os
import sqlite3

pyglet.font.add_file("Pixeboy.ttf")

class Joueur:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.rockets = []

    def move(self, event):
        """ Gestion des évènements clavier : déplacement gauche/droite et tir d'une rocket"""
        c = event.char
        if c == "q": #va a gauche
            self.x -= 10
        if c == "d": #va a droite
            self.x += 10
        if c == " ": #tire
            rocket = Rocket(self.x+15, self.y)
            self.rockets.append(rocket)

    def draw(self, can, skin):
        """ Dessin du vaisseau du joueur dans le canevas"""
        canvasImage = can.create_image(self.x, self.y, image=skin, anchor="nw")

    def getPosition(self):
        """ Renvoi des coordonnées (x, y) du vaisseau du joueur """
        return [self.x, self.y]

    def delRocket(self, rocket):
        """ Suppresion d'une rocket dans le tableau des rockets déja tirées par le joueur """
        self.rockets.remove(rocket)

class Rocket:
    def __init__(self, x, y):
        self.laser_joueur = ImageTk.PhotoImage(file="laser_joueur.png")
        self.x = x
        self.y = y

    def move(self):
        """ Gestion du mouvement de la rocket """
        self.y -= 20
        if self.y >= 0:
            return True
        else:
            return False

    def draw(self, can):
        """ Affichage de la rocket dans le canevas """
        canvasImage = can.create_image(self.x, self.y, image=self.laser_joueur, anchor="nw")

    def hit(self, ennemi):
        """ Méthode permettant de déterminer si la rocket a touché un ennemi """
        if self.x >= ennemi.x and self.x <= ennemi.x+40 and self.y >= ennemi.y and self.y <= ennemi.y+40:
            return True
    
    def hit_boss(self, boss):
        """Méthode permettant de déterminer si la rocket a touché le boss"""
        if self.x >= boss.x and self.x <= boss.x+125 and self.y >= boss.y-5 and self.y <= boss.y+55:
            return True
        
class Boss:
    def __init__(self, x, y):
        self.Boss = ImageTk.PhotoImage(file="Enemie_Boss.png")
        self.x = x
        self.y = y 
    
    def draw(self, can):
        """ Affichage du boss dans le canevas """
        canvasImage = can.create_image(self.x, self.y, image=self.Boss, anchor="nw")
        
    def move(self, rapidité):
        """ Gestion du mouvement du boss """
        self.y += rapidité

class Ennemi:
    def __init__(self, x, y):
        self.ennemi = ImageTk.PhotoImage(file="ennemi1.png")
        self.x = x
        self.y = y

    def draw(self, can):
        """ Affichage de l'ennemi dans le canevas """
        canvasImage = can.create_image(self.x, self.y, image=self.ennemi, anchor="nw")

    def move(self, rapidité):
        """ Gestion du mouvement de l'ennemi """
        self.y += rapidité

    def getPosition(self):
        """ Renvoi des coordonnées (x, y) de l'ennemi """
        return [self.x, self.y]

class Bombe:
    def __init__(self, x, y):
        self.bombe = ImageTk.PhotoImage(file="laser_enemie.png")
        self.x = x
        self.y = y
        self.bombes = []

    def move(self):
        """ Gestion du mouvement de la bombe """
        self.y += 5
        if self.y <= 768:
            return True
        else:
            return False

    def draw(self, can):
        """ Affichage de la bombe dans le canevas """
        canvasImage = can.create_image(self.x, self.y, image=self.bombe, anchor="nw")

    def hit(self, joueur):
        """ Méthode permettant de déterminer si la bombe a touché le joueur """
        if self.x >= joueur.x and self.x <= joueur.x+38 and self.y >= joueur.y-10 and self.y <= joueur.y+30:
            return True
        
class Pièce:
    def __init__(self, x, y):
        self.pièce = ImageTk.PhotoImage(file="gold coin.png")
        self.x = x
        self.y = y
        self.pièces = []

    def move(self):
        """ Gestion du mouvement de la pièce """
        self.y += 5
        if self.y <= 768:
            return True
        else:
            return False

    def draw(self, can):
        """ Affichage de la bombe dans le canevas """
        canvasImage = can.create_image(self.x, self.y, image=self.pièce, anchor="nw")

    def hit(self, joueur):
        """ Méthode permettant de déterminer si la bombe a touché le joueur """
        if self.x >= joueur.x and self.x <= joueur.x+38 and self.y >= joueur.y-10 and self.y <= joueur.y+30:
            return True

class Game:
    def __init__(self):
        self.connexion = sqlite3.connect("Joueurs.db")
        self.pointeur = self.connexion.cursor()
        self.infos_joueur = []
        self.infos_vaisseaux = {'vaisseau1' : False,'vaisseau2' : False,'vaisseau3' : False,'vaisseau4' : False,'vaisseau5' : False,'vaisseau6' : False,'vaisseau7' : False,'vaisseau8' : False,'vaisseau9' : False,'vaisseau10' : False,'vaisseau11' : False,'vaisseau12' : False,'vaisseau13' : False,'vaisseau14' : False}
        self.ennemis = [[],[],[]] # tableau des envahisseurs
        self.pièces = []
        self.bombes = [] # tableau des bombes larguées par les ennemis
        self.boss = [] # tableau du boss
        self.level = 750 # difficulté, les bombes ont 5 chances sur self.level de tomber
        self.round = 3 # assignation du nombre de round qu'on fait en run normal
        self.tour = self.round # nombres de round qu'on modifie par round
        self.rapidité = 0.3 # rapidité des ennemis (en pixels)
        self.vagues = 4 # vagues avant l'arrivée du boss en ranked
        self.flush = self.vagues # nombres de vagues qu'on modifie par vagues
        self.boss_vie = 5 # nombre de coup que doit prendre le boss avant de mourir
        self.boss_coups = self.boss_vie # nombre de coup que doit prendre le boss, mais la on l'utilise pour le modifier
        self.score = 0 # score de départ pour le ranked
        self.meilleurscore = 0 # meilleur score pour le ranked
        self.NMS = False # est ce que durant cette partie, le joueur a fait un nouveau meilleur score
        self.multiplicateur = 1 # multiplicateur de points gagnés par ennemi tué selon la difficulté
        self.NMT = False
        self.crédits = 0
        
        self.décompte = 0
        self.last_kill = 0
        self.kill = 0
        self.temps = 0
        self.meilleur_temps = 0
        self.combo = False
        self.last_combo = 0
        self.nb_combo = 0
        self.multi_combo = 0
        
        self.GO_taille = 12 # taille du titre "Game Over" au départ
        self.NMS_taille = 24 # taille du titre "Nouveau Meilleur score/temps" au départ
        self.tout_grand = False # le titre "Nouveau Meilleur score/temps" a-t-il atteint sa taille max ?

        self.fen = Tk() # fenêtre de jeu
        self.fen.title('Envahisseurs') # titre de la fenêtre
        self.fen.resizable(False, False) # on ne peut pas changer la taille de la fenetre (rend le jeu moche sinon)
        self.can = Canvas(self.fen, width = 1024, height = 768, background='black') # canevas de dessin
        self.can.pack()
        
        self.pseudopris = self.can.create_text(0,0, text="")
        self.codefaux = self.can.create_text(0,0, text="")
        
        # création du joueur
        self.joueur = Joueur(550, 700)  # instanciation du joueur
        
        self.vaisseaux = [ImageTk.PhotoImage(file="vaisseau bleu clair.png"), ImageTk.PhotoImage(file="vaisseau vert.png"), ImageTk.PhotoImage(file="vaisseau bleu foncé.png"), ImageTk.PhotoImage(file="vaisseau rouge.png"), ImageTk.PhotoImage(file="vaisseau gris.png"), ImageTk.PhotoImage(file="vaisseau orange.png"), ImageTk.PhotoImage(file="vaisseau violet.png"), ImageTk.PhotoImage(file="vaisseau jaune.png"), ImageTk.PhotoImage(file="vaisseau rose clair.png"), ImageTk.PhotoImage(file="vaisseau cyan.png"), ImageTk.PhotoImage(file="vaisseau rose.png"), ImageTk.PhotoImage(file="vaisseau avion de chasse.png"), ImageTk.PhotoImage(file="vaisseau vert pomme.png"), ImageTk.PhotoImage(file="vaisseau or.png"), ImageTk.PhotoImage(file="vaisseau blanc.png")]
        self.skin = self.vaisseaux[0]
        self.ppd = ImageTk.PhotoImage(file="pp default.png")
        self.vaisseauchoisi = ""
        
        self.pseudoconnexion = ""
        self.codeconnexion = ""
        self.pseudoinscription = ""
        self.codeinscription = ""
        #5c3821, 4388de, 3c1f7a, cf0000
        self.btn1 = Button(self.fen, text="Continuer", font=("Pixeboy", 48), command=self.Launcher) # bouton de l'écran de fin qui renvoie vers le Launcher
        self.btn2 = Button(self.fen, text="Jouer", font=("Pixeboy", 48),width=8, command=self.restart) # bouton du launcher pour lancer "restart" qui relancera une run normale
        self.btn3 = Button(self.fen, text="Facile", font=("Pixeboy", 36), width=10, state="disabled", command=self.niveau1) # mettre le niveau 1 (niveau de base)
        self.btn4 = Button(self.fen, text="Moyen", font=("Pixeboy", 36), width=10, command=self.niveau2) # mettre le niveau 2
        self.btn5 = Button(self.fen, text="Difficile", font=("Pixeboy", 36), width=10, command=self.niveau3) # mettre le niveau 3
        self.btn6 = Button(self.fen, text="ranked", font=("Pixeboy", 48),width=8, command=self.restart_ranked) # lancer la run ranked
        self.btn7 = Button(self.fen, text="apparence", font=("Pixeboy", 48), command=self.apparences) # ouvrir l'écran de textures
        self.btn8 = Button(self.fen, text="Retour", font=("Pixeboy", 48), command=self.Launcher) # bouton retour qui renvoie vers le Launcher
        self.btn9 = Button(self.fen, image=self.vaisseaux[0], bd=2, bg="#5c3821", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn9, self.vaisseaux[0], "#5c3821"))
        self.btn10 = Button(self.fen, image=self.vaisseaux[1], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn10, self.vaisseaux[1], "#4388de"), state="disable")
        self.btn11 = Button(self.fen, image=self.vaisseaux[2], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn11, self.vaisseaux[2], "#4388de"), state="disable")
        self.btn12 = Button(self.fen, image=self.vaisseaux[3], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn12, self.vaisseaux[3], "#4388de"), state="disable")
        self.btn13 = Button(self.fen, image=self.vaisseaux[4], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn13, self.vaisseaux[4], "#4388de"), state="disable")
        self.btn14 = Button(self.fen, image=self.vaisseaux[5], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn14, self.vaisseaux[5], "#4388de"), state="disable")
        self.btn15 = Button(self.fen, image=self.vaisseaux[6], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn15, self.vaisseaux[6], "#4388de"), state="disable")
        self.btn16 = Button(self.fen, image=self.vaisseaux[7], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn16, self.vaisseaux[7], "#4388de"), state="disable")
        self.btn17 = Button(self.fen, image=self.vaisseaux[8], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn17, self.vaisseaux[8], "#4388de"), state="disable")
        self.btn18 = Button(self.fen, image=self.vaisseaux[9], bd=0, bg="black", activebackground="black", width=72, height=72, command = lambda : self.vaisseau(self.btn18, self.vaisseaux[9], "#4388de"), state="disable")
        self.btn19 = Button(self.fen, image=self.vaisseaux[10], bd=0, bg="black", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn19, self.vaisseaux[10], "#3c1f7a"), state="disable")
        self.btn20 = Button(self.fen, image=self.vaisseaux[11], bd=0, bg="black", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn20, self.vaisseaux[11], "#3c1f7a"), state="disable")
        self.btn21 = Button(self.fen, image=self.vaisseaux[12], bd=0, bg="black", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn21, self.vaisseaux[12], "#3c1f7a"), state="disable")
        self.btn22 = Button(self.fen, image=self.vaisseaux[13], bd=0, bg="black", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn22, self.vaisseaux[13], "#3c1f7a"), state="disable")
        self.btn23 = Button(self.fen, image=self.vaisseaux[14], bd=0, bg="black", activebackground="black", width=72, height=72, command =lambda : self.vaisseau(self.btn23, self.vaisseaux[14], "#cf0000"), state="disable")
        self.btn24 = Button(self.fen, text="Connexion", font=("Pixeboy", 48), command=self.ecranconnection)
        self.btn25 = Button(self.fen, text="se connecter", font=("Pixeboy", 24), width=14, command = self.seconnecter)
        self.btn26 = Button(self.fen, text="s'inscrire", font=("Pixeboy", 24), width=14, command = self.inscription)
        self.btn27 = Button(self.fen, image=self.ppd, width=80, height=80, bd=0, bg="black", activebackground="black", command=self.paramètres)
        self.btn28 = Button(self.fen, text="Acheter une box skin", font=("Pixeboy", 36), command=self.openbox)
        self.btn29 = Button(self.fen, text="Supprimer \nson compte", font=("Pixeboy", 48), bg="red", command=self.delaccount)
        self.btn30 = Button(self.fen, text="Retour", font=("Pixeboy", 48), command=self.apparences) # bouton retour qui renvoie vers le Launcher
        self.btn31 = Button(self.fen, text="Se deconnecter", font=("Pixeboy", 36), command=self.deco)
        self.entree1 = Entry(self.fen, relief="sunken", font=(36))
        self.entree1.insert(0, "pseudo")
        self.entree2 = Entry(self.fen, relief="sunken", font=(36))
        self.entree2.insert(0, "code")
        self.entree3 = Entry(self.fen, relief="sunken", font=(36))
        self.entree3.insert(0, "pseudo")
        self.entree4 = Entry(self.fen, relief="sunken", font=(36))
        self.entree4.insert(0, "code")
        self.entree5 = Entry(self.fen, relief="sunken", font=(36))
        self.entree5.insert(0, "retapez le code")

        self.fen.bind("<Key>", self.joueur.move) # gestion des déplacements clavier du joueur
        self.btnvactivé = self.btn9
        
        self.liste=[ImageTk.PhotoImage(file="vaisseau vert.png"), ImageTk.PhotoImage(file="vaisseau bleu foncé.png"), ImageTk.PhotoImage(file="vaisseau rouge.png"), ImageTk.PhotoImage(file="vaisseau gris.png"), ImageTk.PhotoImage(file="vaisseau orange.png"), ImageTk.PhotoImage(file="vaisseau violet.png"), ImageTk.PhotoImage(file="vaisseau jaune.png"), ImageTk.PhotoImage(file="vaisseau rose clair.png"), ImageTk.PhotoImage(file="vaisseau cyan.png"), ImageTk.PhotoImage(file="vaisseau rose.png"), ImageTk.PhotoImage(file="vaisseau avion de chasse.png"), ImageTk.PhotoImage(file="vaisseau vert pomme.png"), ImageTk.PhotoImage(file="vaisseau or.png"), ImageTk.PhotoImage(file="vaisseau blanc.png")]

        # instanciation des ennemis ( 3 rangées de 10 )
        for i in range(10):
            self.ennemis[0].append(Ennemi(100*i+30, 50))
            self.ennemis[1].append(Ennemi(100*i+30, 100))
            self.ennemis[2].append(Ennemi(100*i+30, 150))

        self.Launcher() # lancement du jeu

        self.fen.mainloop() # boucle principale d'évènements Tkinter
        self.connexion.close()
        
    def run(self):
        self.can.delete('all') # effacement général
        task = self.fen.after(30,self.run) # tache en cours qui rafraichis le jeu toutes les 30millisecondes
        if randint(1, self.level) < 5:
            self.pièces.append(Pièce(randint(12, 1012), 0))
            
        for colonne in self.ennemis: # parcours du 
            for ennemi in colonne: # tableau des ennemis
                ennemi.draw(self.can) # affichage
                ennemi.move(self.rapidité) # déplacement
                if randint(1, self.level) < 5: # larguage d'une bombe avec une probabilité dépendant du niveau
                    x = ennemi.getPosition()[0] # coordonnées de
                    y = ennemi.getPosition()[1] # l'ennemi
                    self.bombes.append(Bombe(x+20, y + 40)) # largage d'une bombe à partir de la position de l'ennemi ( un peu en dessous...)
                if ennemi.y >= 768: # l'ennemi est il arrivé en bas ?
                    self.fen.after_cancel(task) # si oui arrêter le jeu
                    self.Game_Over() # et renvoyer a l'écran Game Over
                
        # affichage vaisseau joueur
        self.joueur.draw(self.can, self.skin)
        
        if self.ennemis == [[],[],[]]: # le tableau des ennemis est il vide 
            self.tour = self.tour - 1 # si oui ca compte comme un round passé
            if self.tour !=0: # si il nous reste des rounds a faire
                # remettre les ennemis dans le tableau
                for i in range(10):
                    self.ennemis[0].append(Ennemi(100*i+30, 50))
                    self.ennemis[1].append(Ennemi(100*i+30, 100))
                    self.ennemis[2].append(Ennemi(100*i+30, 150))
            else:
                self.fen.after_cancel(task) # sinon arrêter la tache en cours et afficher la fenetre de victoire
                self.temps = self.décompte//1000
                if self.meilleur_temps == 0:
                    self.meilleur_temps = self.temps
                    self.NMT = True
                elif self.temps < self.meilleur_temps:
                    self.meilleur_temps = self.temps
                    self.NMT = True
                self.You_Win()
            
        # déplacement des rockets, et test si elles touchent un envahisseur
        for rocket in self.joueur.rockets: # parcours du tableau des rockets déja tirées par le joueur
            rocket.draw(self.can) # affichage de la rocket
            est_tiree = rocket.move() # est-elle toujours en vol ?
            if (not est_tiree): # si non,
                self.joueur.delRocket(rocket) # on la supprime du tableau

            for colonne in self.ennemis:
                for ennemi in colonne: # parcours du tableau des ennemis
                    if rocket.hit(ennemi): # la rocket touche-t-elle l'ennemi ?
                        self.pièces.append(Pièce(ennemi.x, ennemi.y))
                        self.last_kill = self.kill
                        self.kill = self.décompte
                        if self.kill - self.last_kill < 300:
                            self.last_combo = self.nb_combo
                            self.nb_combo = self.kill
                            if self.combo - self.last_combo < 700:
                                self.multi_combo += 1
                            self.combo = True
                        colonne.remove(ennemi) # si oui, on retire l'ennemi de son tableau
                        self.joueur.delRocket(rocket) # et de même pour la rocket

        # idem pour les bombes des ennemis
        for bombe in self.bombes: # pour chaque bombe déja larguée
            bombe.draw(self.can) # affichage de la bombe
            est_tiree = bombe.move() # est-elle toujours en vol ?
            if (not est_tiree): # si non,
                self.bombes.remove(bombe) # on la supprime du tableau

            if bombe.hit(self.joueur): # la bombe touche-t-elle le joueur ?
                self.bombes.remove(bombe) # si oui on la supprime du tableau
                self.fen.after_cancel(task) # on arrête la tache
                self.Game_Over() # et on affiche la fenêtre de Game Over
                
        for pièce in self.pièces: # pour chaque bombe déja larguée
            pièce.draw(self.can) # affichage de la bombe
            est_tiree = pièce.move() # est-elle toujours en vol ?
            if (not est_tiree): # si non,
                self.pièces.remove(pièce) # on la supprime du tableau

            if pièce.hit(self.joueur): # la bombe touche-t-elle le joueur ?
                self.pièces.remove(pièce) # si oui on la supprime du tableau
                self.crédits += 1
                
                
        self.décompte += 30
        if self.décompte - self.kill > 1000:
            self.combo = False
            self.multi_combo = 0
        if self.combo:
            self.com = self.can.create_text(512, 384, text="combo x" + str(self.multi_combo), font=("Pixeboy", 36), fill="yellow")
                
    def run_ranked(self): # c'est fun ca tu vas voir. La plupart des trucs c'est la meme chose que run normal. Je commente que le nouveau
        self.can.delete('all') 
        task_ranked = self.fen.after(30,self.run_ranked)
        if randint(1, self.level) < 5:
            self.pièces.append(Pièce(randint(12, 1012), 0))
            
        for colonne in self.ennemis:
            for ennemi in colonne: 
                ennemi.draw(self.can) 
                ennemi.move(self.rapidité) 
                if randint(1, self.level) < 5: 
                    x = ennemi.getPosition()[0] 
                    y = ennemi.getPosition()[1] 
                    self.bombes.append(Bombe(x+20, y + 40)) #
                if ennemi.y >= 768:
                    self.fen.after_cancel(task_ranked)
                    self.boss.clear() # on clear le tableau du boss
                    if self.score > self.meilleurscore: # si le score de cette partie est un nouveau meilleur score
                        self.meilleurscore = self.score # on assigne le meilleur score au score de cette partie
                        self.NMS = True # on met en True le NMS
                    self.Game_Over()
                
        for boss in self.boss: # parcours du tableau des boss
            boss.draw(self.can) # afficher le boss
            boss.move(self.rapidité) # bouger le boss
                
        self.joueur.draw(self.can, self.skin)
        
        if self.ennemis == [[],[],[]]: 
            self.flush = self.flush - 1
            if self.flush == 0: # si le nombre de vague est écoulé
                self.flush = self.vagues # réinitialiser le nombre de vagues
                self.boss.append(Boss(440, 115)) # ajouter un boss 
            for i in range(10): # remettre les ennemis sur le canevas
                self.ennemis[0].append(Ennemi(100*i+30, 50))
                self.ennemis[1].append(Ennemi(100*i+30, 100))
                self.ennemis[2].append(Ennemi(100*i+30, 150))
            if len(self.boss) == 1: # libérer de la place pour le boss
                self.ennemis[1].remove(self.ennemis[1][4])
                self.ennemis[1].remove(self.ennemis[1][4])
                self.ennemis[2].remove(self.ennemis[2][4])
                self.ennemis[2].remove(self.ennemis[2][4])
            
        for rocket in self.joueur.rockets: 
            rocket.draw(self.can) 
            est_tiree = rocket.move() 
            if (not est_tiree): 
                self.joueur.delRocket(rocket) 
            
            for boss in self.boss: # parcours du tableau du boss
                if rocket.hit_boss(boss): # si la rocket touche le boss
                    self.boss_vie = self.boss_vie - 1 # on lui enlève un point de vie
                    self.joueur.delRocket(rocket) # on retire la rocket
                    if self.boss_vie == 0: # si le boss n'a plus de point de vie
                        for i in range(1*self.multiplicateur):
                            self.pièces.append(Pièce(ennemi.x, ennemi.y))
                        self.boss_vie = self.boss_coups + 2*self.multiplicateur # on lui ajoute de la vie supplémentaire pour la prochaine fois
                        self.boss_coups = self.boss_vie # on réinitialise sa vie
                        self.boss.remove(boss) # et on l'enlève
                        self.score += int(4*self.multiplicateur) # puis on donne des points

            for colonne in self.ennemis:
                for ennemi in colonne: 
                    if rocket.hit(ennemi): 
                        self.pièces.append(Pièce(ennemi.x, ennemi.y))
                        colonne.remove(ennemi) 
                        self.joueur.delRocket(rocket)
                        self.score += int(2*self.multiplicateur) # on donne des points au joueur

        for bombe in self.bombes: 
            bombe.draw(self.can) 
            est_tiree = bombe.move() 
            if (not est_tiree): 
                self.bombes.remove(bombe) 

            if bombe.hit(self.joueur):
                self.bombes.remove(bombe) 
                self.fen.after_cancel(task_ranked)
                self.boss.clear() # on clear le tableau du boss
                if self.score > self.meilleurscore: # si le score de cette partie est un nouveau meilleur score
                    self.meilleurscore = self.score # on assigne le meilleur score au score de cette partie
                    self.NMS = True # on met en True le NMS
                self.Game_Over() # et on renvoie au Game Over
                
        for pièce in self.pièces: # pour chaque bombe déja larguée
            pièce.draw(self.can) # affichage de la bombe
            est_tiree = pièce.move() # est-elle toujours en vol ?
            if (not est_tiree): # si non,
                self.pièces.remove(pièce) # on la supprime du tableau

            if pièce.hit(self.joueur): # la bombe touche-t-elle le joueur ?
                self.pièces.remove(pièce) # si oui on la supprime du tableau
                self.crédits += 1
                
        self.sco = self.can.create_text(90, 25, text="score : " + str(self.score), font=("Pixeboy", 36), fill="white") # on affiche le score au joueur
        
    def Game_Over(self):
        self.can.delete('all') # on vide le canevas
        self.btn2.place(x=1324, y= 626) # on range le bouton 2 loin de la vue
        self.btn3.place(x=1324, y= 676) # ainsi que
        self.btn4.place(x=1324, y= 726) # les boutons
        self.btn5.place(x=1324, y= 776) # 3, 4,
        self.btn6.place(x=1324, y= 826) # 5 et 6
        self.btn7.place(x=1324, y= 876)
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        if self.NMS: # si on a effectué un Nouveau meilleur score
            self.NMSshow = self.can.create_text(512, 450, text="Nouveau meilleur score : " +str(self.meilleurscore), font=("Pixeboy", self.NMS_taille), fill="yellow") # on affiche le beau message en jaune
            if not self.tout_grand: # tant que notre message n'a pas sa taille max
                self.NMS_taille += 2 # on le fait grossir
                if self.NMS_taille == 48: # et si il atteint 48
                    self.tout_grand = True # il a sa taille max
            elif self.tout_grand: # et tant qu'il n'a pas atteint sa taille minimale
                self.NMS_taille -= 2 # on le rappetisse
                if self.NMS_taille == 24: # et quand il est arrivé a sa taille minimale
                    self.tout_grand = False # on relance
        self.bombes.clear() # on nettoie le tableau des bombes
        self.joueur.rockets.clear() # des rockets
        self.ennemis = [[],[],[]] # et des ennemis
        self.btn1.place(x=360.5, y=576) # on affiche le bouton de retour au Launcher
        self.GO = self.can.create_text(512, 300, text="Game Over", font=("Pixeboy", self.GO_taille), fill="white") # et on se moque du joueur
        if self.GO_taille < 108: # on fait grossir le titre de plus en plus jusqu'a ce qu'il atteigne la taille de 108
            self.GO_taille += 3
            self.fen.after(30, self.Game_Over)
        else:
            pass
        self.can.pack()
        
    def You_Win(self):
        self.can.delete('all') # on vide le canevas
        self.btn2.place(x=1324, y= 626) # on range le bouton 2 loin de la vue
        self.btn3.place(x=1324, y= 676) # ainsi que
        self.btn4.place(x=1324, y= 726) # les boutons
        self.btn5.place(x=1324, y= 776) # 3, 4,
        self.btn6.place(x=1324, y= 826) # 5 et 6
        self.btn7.place(x=1324, y= 876)
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        if self.NMT: # si on a effectué un Nouveau meilleur score
            self.NMTshow = self.can.create_text(512, 450, text="Nouveau meilleur temps : " +str(self.meilleur_temps) +"s", font=("Pixeboy", self.NMS_taille), fill="yellow") # on affiche le beau message en jaune
            if not self.tout_grand: # tant que notre message n'a pas sa taille max
                self.NMS_taille += 2 # on le fait grossir
                if self.NMS_taille == 48: # et si il atteint 48
                    self.tout_grand = True # il a sa taille max
            elif self.tout_grand: # et tant qu'il n'a pas atteint sa taille minimale
                self.NMS_taille -= 2 # on le rappetisse
                if self.NMS_taille == 24: # et quand il est arrivé a sa taille minimale
                    self.tout_grand = False # on relance
        self.bombes.clear() # on nettoie le tableau des bombes
        self.joueur.rockets.clear() # des rockets
        self.ennemis = [[],[],[]] # et des ennemis
        self.btn1.place(x=360.5, y=576) # on affiche le bouton de retour au Launcher
        self.YW = self.can.create_text(512, 300, text="You Win", font=("Pixeboy", self.GO_taille), fill="white") # et on félicite le joueur
        if self.GO_taille < 108: # on fait grossir le titre de plus en plus jusqu'a ce qu'il atteigne la taille de 108
            self.GO_taille += 3
            self.fen.after(30, self.You_Win)
        else:
            pass
        self.can.pack()
        
    def Launcher(self):
        if self.infos_joueur != []:
            update1 = """UPDATE joueurs SET crédits = ? WHERE pseudos = ?"""
            self.pointeur.execute(update1, (self.crédits, self.infos_joueur[0][0]))
            update2 = """UPDATE joueurs SET "meilleur temps" = ? WHERE pseudos = ?"""
            self.pointeur.execute(update2, (self.meilleur_temps, self.infos_joueur[0][0]))
            update3 = """UPDATE joueurs SET "meilleur score" = ? WHERE pseudos = ?"""
            self.pointeur.execute(update3, (self.meilleurscore, self.infos_joueur[0][0]))
            self.pointeur.execute('''DELETE FROM vaisseaux WHERE pseudos = ?''', (self.infos_joueur[0][0],))
            donnees2 = list(self.infos_vaisseaux.values())
            donnees2.insert(0, self.infos_joueur[0][0])
            self.pointeur.execute('''INSERT INTO vaisseaux(pseudos,vaisseau1,vaisseau2,vaisseau3,vaisseau4,vaisseau5,vaisseau6,vaisseau7,vaisseau8,vaisseau9,vaisseau10,vaisseau11,vaisseau12,vaisseau13,vaisseau14) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', donnees2 )
            self.connexion.commit()
        self.can.delete('all') # on vide le canevas
        self.btn1.place(x=1324, y=576) # on éloigne le bouton 1
        self.can.pack()
        self.env = self.can.create_text(512, 192, text="envahisseurs", font=("Pixeboy", 108), fill="white") # un texte
        self.niv = self.can.create_text(896, 384, text="niveau", font=("Pixeboy", 36), fill="white") # un autre
        self.MT = self.can.create_text(512, 484, text="meilleur temps : " + str(self.meilleur_temps) +"s", font=("Pixeboy", 36), fill="white") # encore un autre
        self.MS = self.can.create_text(512, 624, text="meilleur score : " + str(self.meilleurscore), font=("Pixeboy", 36), fill="white") # encore un autre
        self.c = self.can.create_text(160, 30, text="pieces : " + str(self.crédits), font=("Pixeboy", 36), fill="white") # encore un autre
        self.btn2.place(x=386, y=384) # et on affiche tous ces boutons sur l'écran
        self.btn3.place(x=790, y=400)
        self.btn4.place(x=790, y=445)
        self.btn5.place(x=790, y=490)
        self.btn6.place(x=386, y=524)
        self.btn7.place(x=40, y=675)
        self.btn8.place(x=1324, y= 926)
        self.btn9.place(x=1324, y= 976)
        self.btn10.place(x=1374, y= 976)
        self.btn11.place(x=1424, y= 976)
        self.btn12.place(x=1474, y= 976)
        self.btn13.place(x=1524, y= 976)
        self.btn14.place(x=1574, y= 976)
        self.btn15.place(x=1624, y= 976)
        self.btn16.place(x=1674, y= 976)
        self.btn17.place(x=1724, y= 976)
        self.btn18.place(x=1774, y= 976)
        self.btn19.place(x=1824, y= 976)
        self.btn20.place(x=1874, y= 976)
        self.btn21.place(x=1924, y= 976)
        self.btn22.place(x=1974, y= 976)
        self.btn23.place(x=2024, y= 976)
        if self.infos_joueur == []:
            self.btn24.place(x=700, y=20)
        else:
            self.btn27.place(x=920, y=20)
        self.btn25.place(x=1324, y= 1076)
        self.btn26.place(x=1324, y= 1126)
        self.btn28.place(x=1324, y=1226)
        self.btn29.place(x=1324, y=1276)
        self.btn31.place(x=1324, y=1326)
        
        self.entree1.place(x=1324, y=76)
        self.entree2.place(x=1324, y=126)
        self.entree3.place(x=1324, y=176)
        self.entree4.place(x=1324, y=226)
        self.entree5.place(x=1324, y=276)
    
    def restart(self): # ca c'est pour relancer le jeu
        self.btn2.place(x=1324, y= 626) # on range les boutons a leur place
        self.btn3.place(x=1324, y= 676)
        self.btn4.place(x=1324, y= 726)
        self.btn5.place(x=1324, y= 776)
        self.btn6.place(x=1324, y= 826)
        self.btn7.place(x=1324, y= 876)
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        self.décompte = 0
        self.NMT = False # on réinitialise le NMT
        # on remplis le tableau des ennemis
        if self.ennemis == [[],[],[]]:
            for i in range(10):
                self.ennemis[0].append(Ennemi(100*i+50, 50))
                self.ennemis[1].append(Ennemi(100*i+50, 100))
                self.ennemis[2].append(Ennemi(100*i+50, 150))
        # on remet le joueur en place
        self.joueur.x = 550
        self.joueur.y = 700
        self.GO_taille = 12 # et on réinitialise ce qui doit être réinitialisé
        self.tour = self.round
        self.run() # et enfin on lance
        
    def restart_ranked(self):
        self.btn2.place(x=1324, y= 626) # on range les boutons
        self.btn3.place(x=1324, y= 676)
        self.btn4.place(x=1324, y= 726)
        self.btn5.place(x=1324, y= 776)
        self.btn6.place(x=1324, y= 826) 
        self.btn7.place(x=1324, y= 876)
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        self.score = 0 # on réinitialise le score
        self.NMS = False # on réinitialise le NMS
        # on remplis le tableau des ennemis
        if self.ennemis == [[],[],[]]:
            for i in range(10):
                self.ennemis[0].append(Ennemi(100*i+50, 50))
                self.ennemis[1].append(Ennemi(100*i+50, 100))
                self.ennemis[2].append(Ennemi(100*i+50, 150))
        # on remet le joueur en place
        self.joueur.x = 550
        self.joueur.y = 700
        self.GO_taille = 12 # on réinitialise ce qui doit être réinitialisé
        self.flush = self.vagues
        self.run_ranked() # et on lance
        
    def apparences(self):
        self.can.delete('all') # on vide le canevas
        self.btn2.place(x=1324, y= 626) # on range les boutons
        self.btn3.place(x=1324, y= 676)
        self.btn4.place(x=1324, y= 726)
        self.btn5.place(x=1324, y= 776)
        self.btn6.place(x=1324, y= 826)
        self.btn7.place(x=1324, y= 876)
        self.btn30.place(x=1324, y= 876)
        self.basic = self.can.create_text(90, 150, text="basique", font=("Pixeboy", 36), fill="white") # un autre
        self.common = self.can.create_text(380, 150, text="communs", font=("Pixeboy", 36), fill="white") # un autre
        self.rare = self.can.create_text(680, 150, text="rares", font=("Pixeboy", 36), fill="white") # un autre
        self.legendary = self.can.create_text(900, 150, text="legendaire", font=("Pixeboy", 36), fill="white") # un autre
        self.btn8.place(x=20, y=10)
        self.btn9.place(x=40, y=200)
        self.btn10.place(x=300, y=200)
        self.btn11.place(x=300, y=300)
        self.btn12.place(x=300, y=400)
        self.btn13.place(x=300, y=500)
        self.btn14.place(x=400, y=200)
        self.btn15.place(x=400, y=300)
        self.btn16.place(x=400, y=400)
        self.btn17.place(x=400, y=500)
        self.btn18.place(x=400, y=600)
        self.btn19.place(x=660, y=200)
        self.btn20.place(x=660, y=300)
        self.btn21.place(x=660, y=400)
        self.btn22.place(x=660, y=500)
        self.btn23.place(x=900, y=200)        
        if self.infos_vaisseaux.get('vaisseau1'):
            self.btn10.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau2'):
            self.btn11.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau3'):
            self.btn12.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau4'):
            self.btn13.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau5'):
            self.btn14.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau6'):
            self.btn15.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau7'):
            self.btn16.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau8'):
            self.btn17.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau9'):
            self.btn18.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau10'):
            self.btn19.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau11'):
            self.btn20.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau12'):
            self.btn21.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau13'):
            self.btn22.configure(state="active")
        if self.infos_vaisseaux.get('vaisseau14'):
            self.btn23.configure(state="active")
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        self.btn28.place(x=580, y=700)
        
    def ecranconnection(self, test=""):
        self.can.delete('all') # on vide le canevas
        self.textconn = self.can.create_text(200, 150, text="se connecter", font=("Pixeboy", 36), fill="white") # un autre
        self.textou = self.can.create_text(550, 150, text="ou", font=("Pixeboy", 36), fill="white") # un autre
        self.textinsc = self.can.create_text(820, 150, text="s'inscrire", font=("Pixeboy", 36), fill="white") # un autre
        self.btn2.place(x=1324, y= 626) # on range les boutons
        self.btn3.place(x=1324, y= 676)
        self.btn4.place(x=1324, y= 726)
        self.btn5.place(x=1324, y= 776)
        self.btn6.place(x=1324, y= 826)
        self.btn7.place(x=1324, y= 876)
        self.btn8.place(x=20, y=10)
        self.btn24.place(x=1324, y=1026)
        self.entree1.place(x=80, y=200)
        self.entree2.place(x=80, y=250)
        self.entree3.place(x=700, y=200)
        self.entree4.place(x=700, y=250)
        self.entree5.place(x=700, y=300)
        self.btn25.place(x=80, y=300)
        self.btn26.place(x=700, y= 350)
        
    def seconnecter(self):
        self.pointeur.execute('''SELECT pseudos, code FROM joueurs''')
        pseudo = self.entree1.get()
        boncompte=False
        code = self.entree2.get()
        liste_infos = self.pointeur.fetchall()
        for j in liste_infos:
            if pseudo == j[0] and code == j[1]:
                boncompte = True
        if boncompte:
            self.pointeur.execute('''SELECT * FROM joueurs WHERE pseudos = ?''', (pseudo,))
            self.infos_joueur = self.pointeur.fetchall()
            self.crédits = self.infos_joueur[0][2]
            self.meilleur_temps = self.infos_joueur[0][3]
            self.meilleurscore = self.infos_joueur[0][4]
            self.pointeur.execute('''SELECT * FROM vaisseaux WHERE pseudos= ?''', (pseudo,))
            liste_vaisseaux = self.pointeur.fetchall()
            i=1
            for k in self.infos_vaisseaux:
                self.infos_vaisseaux[k] = liste_vaisseaux[0][i]
                i+=1
            self.Launcher()
        else:
            self.souciconnexion = self.can.create_text(510, 500, text="Connexion impossible :\nVotre pseudo ou votre code est incorrect. Verifiez \nque vous ayez tape les bonnes informations", font=("Pixeboy", 36), fill="red") # un autre
        
    def inscription(self):
        self.pointeur.execute('''SELECT pseudos FROM joueurs''')
        self.can.delete(self.pseudopris)
        self.can.delete(self.codefaux)
        inscription_bonne = True
        pseudo = self.entree3.get()
        code1 = self.entree4.get()
        code2 = self.entree5.get()
        liste_infos = self.pointeur.fetchall()
        donnees = (str(pseudo), str(code1), self.crédits, self.meilleur_temps, self.meilleurscore)
        for j in liste_infos:
            if pseudo == j[0]:
                self.pseudopris = self.can.create_text(510, 500, text="Inscription impossible :\nCe pseudo est deja pris", font=("Pixeboy", 36), fill="red") # un autre
                inscription_bonne = False
            elif code1 != code2:
                self.codefaux = self.can.create_text(510, 600, text="Inscription impossible :\nLes deux codes sont differents", font=("Pixeboy", 36), fill="red") # un autre
                inscription_bonne = False
        if inscription_bonne:
            self.pointeur.execute('''INSERT INTO joueurs(pseudos, code, crédits, "meilleur temps", "meilleur score") VALUES( ?, ?, ?, ?, ?)''' ,donnees)
            donnees2 = list(self.infos_vaisseaux.values())
            donnees2.insert(0, pseudo)
            self.pointeur.execute('''INSERT INTO vaisseaux(pseudos,vaisseau1,vaisseau2,vaisseau3,vaisseau4,vaisseau5,vaisseau6,vaisseau7,vaisseau8,vaisseau9,vaisseau10,vaisseau11,vaisseau12,vaisseau13,vaisseau14) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', donnees2 )
            self.connexion.commit()
            self.pointeur.execute('''SELECT * FROM joueurs WHERE pseudos = ?''', (pseudo,))
            self.infos_joueur = self.pointeur.fetchall()
            self.Launcher()
            
    def skinbox(self):
        task_box = self.fen.after(600,self.skinbox)
        self.can.delete('all')
        liste=self.liste
        shuffle(liste) #on fait bouger les images des vaisseaux pour un meilleur rendu
        canvasImage = self.can.create_image(10, 384, image=liste[0], anchor="nw")
        canvasImage = self.can.create_image(85, 384, image=liste[1], anchor="nw")
        canvasImage = self.can.create_image(160, 384, image=liste[2], anchor="nw")
        canvasImage = self.can.create_image(235, 384, image=liste[3], anchor="nw")
        canvasImage = self.can.create_image(310, 384, image=liste[4], anchor="nw")
        canvasImage = self.can.create_image(385, 384, image=liste[5], anchor="nw")
        canvasImage = self.can.create_image(460, 384, image=liste[6], anchor="nw")
        canvasImage = self.can.create_image(535, 384, image=liste[7], anchor="nw")
        canvasImage = self.can.create_image(610, 384, image=liste[8], anchor="nw")
        canvasImage = self.can.create_image(685, 384, image=liste[9], anchor="nw")
        canvasImage = self.can.create_image(760, 384, image=liste[10], anchor="nw")
        canvasImage = self.can.create_image(835, 384, image=liste[11], anchor="nw")
        canvasImage = self.can.create_image(910, 384, image=liste[12], anchor="nw")
        canvasImage = self.can.create_image(985, 384, image=liste[13], anchor="nw")
        self.temps+=600
        if self.temps==6000:
            self.fen.after_cancel(task_box)
            self.temps=0
            self.can.delete('all')
            indice = list(self.infos_vaisseaux).index(self.vaisseauchoisi)
            canvasImage = self.can.create_image(512, 382, image=self.vaisseaux[indice+1], anchor="nw")
            self.btn30.place(x=20, y=10)
    
    def openbox(self):
        if self.crédits <60:
            self.pasassez = self.can.create_text(280, 730, text="Il vous faut 60 pieces", font=("Pixeboy", 36), fill="red") # un autre
        else:
            self.temps = 0
            self.crédits-=60
            self.btn2.place(x=1324, y= 626) # on range les boutons
            self.btn3.place(x=1324, y= 676)
            self.btn4.place(x=1324, y= 726)
            self.btn5.place(x=1324, y= 776)
            self.btn6.place(x=1324, y= 826)
            self.btn7.place(x=1324, y= 876)
            self.btn8.place(x=1324, y= 926)
            self.btn9.place(x=1324, y= 976)
            self.btn10.place(x=1374, y= 976)
            self.btn11.place(x=1424, y= 976)
            self.btn12.place(x=1474, y= 976)
            self.btn13.place(x=1524, y= 976)
            self.btn14.place(x=1574, y= 976)
            self.btn15.place(x=1624, y= 976)
            self.btn16.place(x=1674, y= 976)
            self.btn17.place(x=1724, y= 976)
            self.btn18.place(x=1774, y= 976)
            self.btn19.place(x=1824, y= 976)
            self.btn20.place(x=1874, y= 976)
            self.btn21.place(x=1924, y= 976)
            self.btn22.place(x=1974, y= 976)
            self.btn23.place(x=2024, y= 976)
            self.btn24.place(x=1324, y=1026)
            self.btn27.place(x=1324, y=1176)
            self.btn28.place(x=1324, y=1226)
            skin = randint(1,75) #on prend un chiffre entre 1 et 75 qui définira le skin recu. 18/25 chance d'éavoir un skin commun, 1/15 d'avoir un rare et 1/75 d'avoir un légendaire
            if 1<=skin<=6:
                self.vaisseauchoisi = 'vaisseau1'
                if self.infos_vaisseaux.get('vaisseau1'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau1'] = True
            if 7<=skin<=12:
                self.vaisseauchoisi = 'vaisseau2'
                if self.infos_vaisseaux.get('vaisseau2'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau2'] = True
            if 13<=skin<=18:
                self.vaisseauchoisi = 'vaisseau3'
                if self.infos_vaisseaux.get('vaisseau3'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau3'] = True
            if 19<=skin<=24:
                self.vaisseauchoisi = 'vaisseau4'
                if self.infos_vaisseaux.get('vaisseau4'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau4'] = True
            if 25<=skin<=30:
                self.vaisseauchoisi = 'vaisseau5'
                if self.infos_vaisseaux.get('vaisseau5'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau5'] = True
            if 31<=skin<=36:
                self.vaisseauchoisi = 'vaisseau6'
                if self.infos_vaisseaux.get('vaisseau6'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau6'] = True
            if 37<=skin<=42:
                self.vaisseauchoisi = 'vaisseau7'
                if self.infos_vaisseaux.get('vaisseau7'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau7'] = True
            if 43<=skin<=48:
                self.vaisseauchoisi = 'vaisseau8'
                if self.infos_vaisseaux.get('vaisseau8'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau8'] = True
            if 49<=skin<=54:
                self.vaisseauchoisi = 'vaisseau9'
                if self.infos_vaisseaux.get('vaisseau9'):
                    self.crédits += 15
                else:
                    self.infos_vaisseaux['vaisseau9'] = True
            if 55<=skin<=59:
                self.vaisseauchoisi = 'vaisseau10'
                if self.infos_vaisseaux.get('vaisseau10'):
                    self.crédits += 30
                else:
                    self.infos_vaisseaux['vaisseau10'] = True
            if 60<=skin<=64:
                self.vaisseauchoisi = 'vaisseau11'
                if self.infos_vaisseaux.get('vaisseau11'):
                    self.crédits += 30
                else:
                    self.infos_vaisseaux['vaisseau11'] = True
            if 65<=skin<=69:
                self.vaisseauchoisi = 'vaisseau12'
                if self.infos_vaisseaux.get('vaisseau12'):
                    self.crédits += 30
                else:
                    self.infos_vaisseaux['vaisseau12'] = True
            if 70<=skin<=74:
                self.vaisseauchoisi = 'vaisseau13'
                if self.infos_vaisseaux.get('vaisseau13'):
                    self.crédits += 30
                else:
                    self.infos_vaisseaux['vaisseau13'] = True
            if skin==75:
                self.vaisseauchoisi = 'vaisseau14'
                if self.infos_vaisseaux.get('vaisseau14'):
                    self.crédits += 60
                else:
                    self.infos_vaisseaux['vaisseau14'] = True
            self.skinbox()
    
    def paramètres(self):
        self.can.delete('all')
        self.btn2.place(x=1324, y= 626) # on range les boutons
        self.btn3.place(x=1324, y= 676)
        self.btn4.place(x=1324, y= 726)
        self.btn5.place(x=1324, y= 776)
        self.btn6.place(x=1324, y= 826)
        self.btn7.place(x=1324, y= 876)
        self.btn9.place(x=1324, y= 976)
        self.btn10.place(x=1374, y= 976)
        self.btn11.place(x=1424, y= 976)
        self.btn12.place(x=1474, y= 976)
        self.btn13.place(x=1524, y= 976)
        self.btn14.place(x=1574, y= 976)
        self.btn15.place(x=1624, y= 976)
        self.btn16.place(x=1674, y= 976)
        self.btn17.place(x=1724, y= 976)
        self.btn18.place(x=1774, y= 976)
        self.btn19.place(x=1824, y= 976)
        self.btn20.place(x=1874, y= 976)
        self.btn21.place(x=1924, y= 976)
        self.btn22.place(x=1974, y= 976)
        self.btn23.place(x=2024, y= 976)
        self.btn24.place(x=1324, y=1026)
        self.btn27.place(x=1324, y=1176)
        self.btn28.place(x=1324, y=1226)
        self.btn8.place(x=20, y=10)
        self.btn29.place(x=352, y=282)
        self.btn31.place(x=347, y=482)
        
    def remplace_placeholder(self, entree, texte):
        entree.delete(0, END)
        entree.insert(0, texte)
        
    def deactivatevaisseau(self):
        self.btn10.configure(state="disabled")
        self.btn11.configure(state="disabled")
        self.btn12.configure(state="disabled")
        self.btn13.configure(state="disabled")
        self.btn14.configure(state="disabled")
        self.btn15.configure(state="disabled")
        self.btn16.configure(state="disabled")
        self.btn17.configure(state="disabled")
        self.btn18.configure(state="disabled")
        self.btn19.configure(state="disabled")
        self.btn20.configure(state="disabled")
        self.btn21.configure(state="disabled")
        self.btn22.configure(state="disabled")
        self.btn23.configure(state="disabled")
        
    def delaccount(self):
        self.infos_vaisseaux = self.infos_vaisseaux = {'vaisseau1' : False,'vaisseau2' : False,'vaisseau3' : False,'vaisseau4' : False,'vaisseau5' : False,'vaisseau6' : False,'vaisseau7' : False,'vaisseau8' : False,'vaisseau9' : False,'vaisseau10' : False,'vaisseau11' : False,'vaisseau12' : False,'vaisseau13' : False,'vaisseau14' : False}
        self.pointeur.execute('''DELETE FROM joueurs WHERE pseudos = ?''',(self.infos_joueur[0][0],))
        self.pointeur.execute('''DELETE FROM vaisseaux WHERE pseudos = ?''',(self.infos_joueur[0][0],))
        self.connexion.commit()
        self.infos_joueur = []
        self.deactivatevaisseau()
        self.Launcher()
        self.remplace_placeholder(self.entree1, "pseudo")
        self.remplace_placeholder(self.entree2, "code")
        self.remplace_placeholder(self.entree3, "pseudo")
        self.remplace_placeholder(self.entree4, "code")
        self.remplace_placeholder(self.entree5, "retapez le code")
        
    def deco(self):
        self.infos_vaisseaux = self.infos_vaisseaux = {'vaisseau1' : False,'vaisseau2' : False,'vaisseau3' : False,'vaisseau4' : False,'vaisseau5' : False,'vaisseau6' : False,'vaisseau7' : False,'vaisseau8' : False,'vaisseau9' : False,'vaisseau10' : False,'vaisseau11' : False,'vaisseau12' : False,'vaisseau13' : False,'vaisseau14' : False}
        self.infos_joueur = []
        self.crédits = 0
        self.meilleur_temps = 0
        self.meilleurscore = 0
        self.deactivatevaisseau()
        self.Launcher()
        self.remplace_placeholder(self.entree1, "pseudo")
        self.remplace_placeholder(self.entree2, "code")
        self.remplace_placeholder(self.entree3, "pseudo")
        self.remplace_placeholder(self.entree4, "code")
        self.remplace_placeholder(self.entree5, "retapez le code")
        
    def niveau1(self): # niveau facile
        self.level = 750 # 1 chance sur 150 pour l'ennemi de drop une bombe
        self.round = 3 # 3 rounds pour une run normale
        self.rapidité = 0.3 # vitesse des ennemis lente
        self.multiplicateur = 1 # les points en ranked sont en x1
        self.vagues = 4 # 4 rounds avant le boss pour la run ranked 
        self.btn3.configure(state="disabled") # on met ce bouton en gris et le reste en blanc
        self.btn4.configure(state="active")
        self.btn5.configure(state="active")
    
    def niveau2(self):
        self.level = 500 # 1 chance sur 100 pour l'ennemi de drop une bombe
        self.round = 5 # 5 rounds pour une run normale
        self.rapidité = 0.5 # vitesse des ennemis moyenne
        self.multiplicateur = 1.5 # les points en ranked sont en x1.5
        self.vagues = 7 # 7 rounds avant le boss pour la run ranked
        self.btn3.configure(state="active")
        self.btn4.configure(state="disabled") # on met ce bouton en gris et le reste en blanc
        self.btn5.configure(state="active")
        
    def niveau3(self):
        self.level = 250 # 1 chance sur 50 pour l'ennemi de drop une bombe
        self.round = 8 # 8 rounds pour une run normale
        self.rapidité = 0.7 # vitesse des ennemis rapide
        self.multiplicateur = 2 # les points en ranked sont en x2
        self.vagues = 9 # 9 rounds avant le boss pour la run ranked
        self.btn3.configure(state="active")
        self.btn4.configure(state="active")
        self.btn5.configure(state="disabled") # on met ce bouton en gris et le reste en blanc
        
    def vaisseau(self, bouton, vaisseau, couleur):#en choisissant ce vaisseau, on rend noir le fond de tous les autres et on colore son fond
        self.skin = vaisseau
        self.btnvactivé.configure(bd=0, bg="black")
        self.btnvactivé = bouton
        bouton.configure(bd=2, bg=couleur) 