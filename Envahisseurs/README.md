Comment l'installer : 
- Vérifier que vous ayez une version de Python plus récente que la 3.8
- Installer la bibliothèque Pillow : 
	python3 -m pip install --upgrade pip
	python3 -m pip install --upgrade Pillow
- Installer la bibliothèque Pyglet : 
	pip install --upgrade --user pyglet

Comment lancer le jeu : 
- Ouvrir le dossier sources
- Ouvrir le fichier envahisseurs
- Executez ce-même fichier
- Dans la console lancez la commande Game()