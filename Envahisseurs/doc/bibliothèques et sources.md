Les bibliothèques utilisées pour ce projet sont Pillow, Pyglet et Tkinter. Nous utilisons aussi la police d'écriture Pixeboy. Toutes les images utilsées mis à part celle de la pièce et
celle de la photo de profil par défaut du compte ont été faites par notre équipe. Les deux images citées ont été trouvées sous license libre sur Google.

Tout le code présent dans le programme à la date du 29 Mars 2024 a été écrit par l'équipe du Jeu Envahisseurs, Thomas PHILIPPE et Yanis BOULY. Nous n'avons copié ni plagié aucun code 
présent sur Internet. 