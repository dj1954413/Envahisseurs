Après avoir lancé le jeu, vous arriverez à l'écran d'Accueil. 

![alt text](image.png)

Sur cette interface, vous trouverez une multitude de boutons.
Les deux boutons principaux sont ceux-ci : 

![alt text](image-1.png)

Le bouton Jouer lancera une partie avec un nombre de vague déterminé. Votre but est de détruire les vaisseaux ennemis. Pour faire bouger votre personnage, vous n'avez qu'a appuyer sur "q" pour vous déplacer sur la gauche, et "d" pour vous déplacer sur la droite. Pour tirer, il vous suffit d'appuyer sur votre barre espace. 

![alt text](image-2.png)

Si vous êtes touché par un tir ennemi, vous serez renvoyé vers l'écran de Game Over.

![alt text](image-3.png)

Par contre, si vous réussissez a survire le nombre défini de vagues, vous arriverez a l'écran de Victoire, avec, si vous avez battu votre meilleur temps, votre nouveau meilleur temps (à noter que si vous ne réussissez pas à atteindre cet écran de victoire, votre meilleur temps restera le même):

![alt text](image-4.png)

Le second bouton, ranked, lancera une partie avec un nombre infini de vague. Votre but est de survivre le plus longtemps en détruisant le plus d'ennemis. Au bout d'un nombre défini de vagues, un Boss apparaitra. Il sera plus dur a détruire mais plus rentable en terme de points. 

![alt text](image-5.png)

Vous avez ensuite 3 boutons permettant de gérer la difficulté de vos parties. La difficulté définira la vitesse de tir et de mouvement des ennemis, mais aussi les points et les pièces donnés par la destruction d'un ennemi. La difficulté définira aussi le nombre de vagues pour gagner ou atteindre le niveau du boss.

![alt text](image-6.png)

En haut à gauche vous avez un compteur de pièces que vous pouvez gagner en jouant et qui vous seront utiles pour acheter de nouvelles apparences pour votre vaisseau.

![alt text](image-7.png)

En parlant d'apparence, en bas à gauche vous verrez un bouton "apparences".

![alt text](image-8.png)

Celui ci vous emmènera à l'interface de changement d'apparence.

![alt text](image-9.png)

Comme vous le voyez, au départ, vous n'avez aucune apparence de débloquée. Pour en débloquer une, il vous suffit d'acheter une box de chance pour 60 pièces pour en gagner un. Plus le grade du vaisseau est rare, plus compliqué il sera à avoir. Un message d'erreur apparaitra si vous tentez d'acheter une box sans avoir le nombre de pièces requis. Si vous tombez par malchance sur une apparence déjà débloquée, vous serez remboursé une partie du paiement. 15 pièces pour une apparence commune, 30 pour une apparence rare et la totale pour l'apparendre légendaire.

![alt text](image-10.png)

Pour finir avec l'écran d'Accueil, en haut à droite vous pouvez voir un bouton de connexion.

![alt text](image-11.png)

Celui-ci ouvrira l'interface de connexion où d'inscription.

![alt text](image-12.png)

Pour s'inscrire, il vous faut entrer un pseudonyme non-utilisé et deux fois le même mot de passe. Si une erreur a été commise, un message d'erreur s'affichera.
Pour se connecter, il suffit d'entrer son pseudonyme et son code. Après vous être inscrit ou connecté, vous serez renvoyé vers l'Accueil.
Si vous êtes connecté à un compte, le bouton de connexion dans l'interface de l'accueil sera remplacé par une photo de profil par défaut :

![alt text](image-13.png)

En appuyant sur ce bouton, vous serez envoyé à une interface de gestion du compte. Pour l'instant, cette interface ne comporte que le bouton supprimer son compte ou se déconnecter du compte. Les actions des deux boutons sont assez explicite.

![alt text](image-14.png)